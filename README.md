# neovim-lua-rc

## Installation

    ./install_symlinks.sh
    nvim +PackerInstall

## Tip

### Remote Neovim headless server

<https://codeberg.org/ashwinvis/ssh.nvim>
