#!/bin/bash
set -xe
FORCE=""  # "-f"
mkdir -p ~/.config/nvim
rm $FORCE ~/.config/nvim/init.vim
ln -rs $FORCE ./config/nvim/lua ~/.config/nvim/lua
ln -rs $FORCE ./config/nvim/plugin ~/.config/nvim/plugin
ln -rs $FORCE ./config/nvim/init.lua ~/.config/nvim/init.lua
rm $FORCE ~/.local/share/nvim/site/pack
# ln -rs $FORCE ./data/nvim/site/pack ~/.local/share/nvim/site/pack
