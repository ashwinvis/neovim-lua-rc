-----------------------------------------------------------
-- Plugin manager configuration file
-----------------------------------------------------------

-- Plugin manager: lazy.nvim
-- url: https://github.com/folke/lazy.nvim

-- BOOTSTRAP
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- CONFIG
-- Note: old plugin config set for removal
-- require('plugins/nvim-tree')
-- require("plugins/feline")
-- require('plugins/vista')
-- require('plugins/nvim-cmp')
-- require("plugins/renamer-nvim")
-- require('plugins/auto_session')

local lazy_opts = {}

-- Add packages
local lazy_plugins = {
  -- Dashboard (start screen)
  {
    "goolord/alpha-nvim",
    config = function()
      require("plugins/alpha-nvim")
    end,
  },

  -- Indent line
  {
    "lukas-reineke/indent-blankline.nvim",
    event = { "BufReadPost", "BufNewFile" },
    config = function()
      require("plugins/indent-blankline")
    end,
  },

  -- Autopair
  {
    "windwp/nvim-autopairs",
    config = true,
    -- event = "InsertEnter",
    ft={"json", "sh"}  -- having autopairs always on messes with coq_nvim's autocomplete
  },

  -- Surround
  {
    "kylechui/nvim-surround",
    event = { "BufReadPre", "BufNewFile" },
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    config = true
  },

  -- Smooth scrolling
  {
    'karb94/neoscroll.nvim',
    config = true,
  },

  -- Tag viewer
  -- use 'liuchengxu/vista.vim'

  -- Treesitter interface
  {
    "nvim-treesitter/nvim-treesitter",
    event = { "BufReadPre", "BufNewFile" },
    config = function()
      require("plugins/nvim-treesitter")
    end,
    build = function()
      require("nvim-treesitter")
      vim.cmd("TSUpdate")
    end
  },

  -- Color schemes
  { "navarasu/onedark.nvim", lazy=true },
  { "EdenEast/nightfox.nvim", lazy=true },

  -- LSP
  {
    "neovim/nvim-lspconfig",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      "jose-elias-alvarez/null-ls.nvim",
      "folke/trouble.nvim",
    },
    config = function()
      require("mason").setup()
      require("mason-lspconfig").setup({
        ensure_installed = {
          -- "cssls",
          "lua_ls",
          "rust_analyzer",
          -- "jsonls",
          -- "remark_ls",
          "pylsp",
          -- "jedi_language_server",
          "ruff_lsp",
        },
      })
      require("plugins/nvim-lspconfig")
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim",
    lazy = true,
    build = function()
      require("mason")
      require("mason-lspconfig")
      vim.cmd("PylspInstall python-lsp-ruff pyls-mypy")
    end
  },
  {
    "jose-elias-alvarez/null-ls.nvim",
    lazy = true,
    pin = true, -- archived, need to find a replacement
    config = function()
      require("plugins/null-ls")
    end,
  },
  {
    "folke/trouble.nvim",
    lazy = true,
    config = function()
      require("plugins/trouble")
    end,
    dependencies = { "nvim-tree/nvim-web-devicons" },
  },

  -- Rust
  -- {
  --   'simrat39/rust-tools.nvim',
  --   config = function()
  --      require('plugins/rust-tools')
  --   end,
  --   dependencies = {
  --     -- Debugging
  --     'nvim-lua/plenary.nvim',
  --     'mfussenegger/nvim-dap'
  --   },
  -- },

  -- Autocomplete
  --[[
  {
    'hrsh7th/nvim-cmp',
    dependencies = {
      'L3MON4D3/LuaSnip',
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-buffer',
      'saadparwaiz1/cmp_luasnip',
    },
  }
  ]]

  {
    "ms-jpq/coq_nvim",
    event = { "BufReadPre", "BufNewFile" },
    branch = "coq",
    dependencies = {
      {
        "ms-jpq/coq.artifacts",
        branch = "artifacts",
      },
      {
        "ms-jpq/coq.thirdparty",
        branch = "3p",
      },
    },
    build = ":COQdeps",
  },

  -- Statusline
  {
    "nvim-lualine/lualine.nvim",
    config = function()
      require("plugins/lualine")
    end,
    dependencies = { "nvim-tree/nvim-web-devicons", lazy = true },
  },

  {
    "nvim-lua/plenary.nvim",
    lazy = true,
  },

  -- git labels
  {
    "lewis6991/gitsigns.nvim",
    event = { "BufReadPre", "BufNewFile" },
    dependencies = { "nvim-lua/plenary.nvim" },
    config = true,
  },

  -- Docstrings
  {
    "danymat/neogen",
    event = { "BufReadPost", "BufNewFile" },
    dependencies = "nvim-treesitter/nvim-treesitter",
    config = function()
      require("plugins/neogen")
    end,
    -- Uncomment next line if you want to follow only stable versions
    version = "*",
  },

  -- Comment
  {
    "numToStr/Comment.nvim",
    event = { "BufReadPost", "BufNewFile" },
    config = function()
      require("plugins/comment")
    end,
  },

  -- Git
  {
    "NeogitOrg/neogit",
    keys = {
      "<leader>g",
    },
    cmd = "Neogit",
    dependencies = {
      "sindrets/diffview.nvim",
    },
    config = function()
      require("plugins/neogit")
    end,
  },

  {
    "sindrets/diffview.nvim",
    lazy = true,
    keys = {
      { "<leader>do", "<cmd>DiffviewOpen<cr>",  desc = "Diffview: open" },
      { "<leader>dc", "<cmd>DiffviewClose<cr>", desc = "Diffview: close" },
    },
  },

  -- VSCode like experience, but with <leader>key
  {
    "nvim-telescope/telescope.nvim",
    -- cmd = "Telescope",
    -- lazy = true,
    config = function()
      require("plugins/telescope")
    end,
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope-file-browser.nvim",
      "nvim-telescope/telescope-frecency.nvim",
    },
  },

  {
    "nvim-telescope/telescope-frecency.nvim",
    lazy = true,
    dependencies = {
      { "tami5/sqlite.lua", lazy = true },
    },
  },

  -- File explorer
  {
    "nvim-telescope/telescope-file-browser.nvim",
    lazy = true,
    config = function()
      require("telescope").load_extension("file_browser")
    end,
  },
  -- {'nvim-tree/nvim-tree.lua'},

  -- VSCode like refactoring
  {
    "ThePrimeagen/refactoring.nvim",
    event = { "BufReadPre", "BufNewFile" },
    config = function()
      require("plugins/refactoring")
    end,
    dependencies = {
      { "nvim-lua/plenary.nvim" },
      { "nvim-treesitter/nvim-treesitter" },
    },
  },

  -- Terminal
  {
    "akinsho/toggleterm.nvim",
    version = "*",
    config = function()
      require("plugins/toggleterm")
    end,
  },

  -- Lua inspect
  {
    "kikito/inspect.lua",
    build = {
      "cd " .. vim.fn.stdpath("data") .. "/lazy/inspect.lua",
      "mkdir -p lua/inspect ",
      "cp -f inspect.lua ./lua/inspect/init.lua",
    },
  },

  -- Window pane moving like tiling wm. Command :WinShift
  {
    "sindrets/winshift.nvim",
    event = { "BufReadPre", "BufNewFile" },
    config = function()
      require("core").map("n", "<leader>m", "<Cmd>WinShift<CR>", { desc = "WinShift: Start Win-Move mode" })
    end,
  },
}

require("lazy").setup(lazy_plugins, lazy_opts)
