-----------------------------------------------------------
-- Define keymaps of Neovim and installed plugins.
-----------------------------------------------------------
local map = require("core").map

-- Change leader to a comma
vim.g.mapleader = ','

-----------------------------------------------------------
-- Editor improvements
-----------------------------------------------------------

map('n', '<leader>"', 'ciW"<C-R>""<ESC>', {desc='double quote a whole word'})

-----------------------------------------------------------
-- Neovim shortcuts
-----------------------------------------------------------

-- Clear search highlighting with <leader> and c
map('n', '<leader>c', ':nohl<CR>')

-- Map Esc to kk
-- map('i', 'kk', '<Esc>')

-- Don't use arrow keys
-- map('', '<up>', '<nop>')
-- map('', '<down>', '<nop>')
-- map('', '<left>', '<nop>')
-- map('', '<right>', '<nop>')

-- Fast saving with <leader> and s
map('n', '<leader>s', ':w<CR>')
map('i', '<C-s>', '<C-o>:w<CR>')

-- Move around splits using Ctrl + {h,j,k,l}
map('n', '<C-h>', '<C-w>h')
map('n', '<C-j>', '<C-w>j')
map('n', '<C-k>', '<C-w>k')
map('n', '<C-l>', '<C-w>l')

-- Close current windows and exit from Neovim with <leader> and q
map('n', '<leader>q', ':bd<CR>')
map('n', '<leader>Q', ':qa<CR>')

-- Tabs
map('n', '<C-t>', ':tabnew<CR>')                      -- open

-- Change local cwd to buffer's parent directory
map('n', '<leader>cd', ':lcd %:h<CR>')

-- Close and open folds
map('n', '<SPACE>z', 'za')

-----------------------------------------------------------
-- Applications and Plugins shortcuts
-----------------------------------------------------------

-- Terminal mappings
-- NOTE: use F4 for toggle term instead
-- map('n', '<C-t>', ':Term<CR>')                      -- open
map('t', '<Esc>', '<C-\\><C-n>')                    -- exit


-- Vista tag-viewer
-- map('n', '<C-m>', ':Vista!!<CR>') -- open/close

-- Alpha
map('n', '<leader>aa', ':Alpha<CR>', {desc="Open Alpha startup page"})

-- {{{vim-unimpaired like keybindings
map('n', "]b",  ":bnext<CR>")
map('n', "[b",  ":bprev<CR>")
map('n', "]B",  ":blast<CR>")
map('n', "[B",  ":bfirst<CR>")
map('n', "]q",  ":cnext<CR>")
map('n', "[q",  ":cprev<CR>")
map('n', "]Q",  ":clast<CR>")
map('n', "[Q",  ":cfirst<CR>")
map('n', "]t",  ":tabn<CR>")
map('n', "[t",  ":tabp<CR>")
map('n', "]T",  ":tablast<CR>")
map('n', "[T",  ":tabfirst<CR>")
-- map('n', "]t",  ":tnext<CR>")
-- map('n', "[t",  ":tprev<CR>")
-- map('n', "]T",  ":tlast<CR>")
-- map('n', "[T",  ":tfirst<CR>")
map('n', "]on", ":set relativenumber<CR>")
map('n', "[on", ":set norelativenumber<CR>")
map('n', "]os", ":set spell<CR>")
map('n', "[os", ":set nospell<CR>")
-- }}}

-- Window split horizontal and vertical
map('n', '<leader>vv', '<C-w>t<C-w>H', {desc="Switch layout to vertical splits"})
map('n', '<leader>hh', '<C-w>t<C-w>K', {desc="Switch layout to horizontal splits"})

------------------------------------------------------------
-- Custom Functions
------------------------------------------------------------
-- Open file at specific line and column number
local function GotoFileLineColumn()
    -- Get the filename, line number, and column number from the current line
    local line_info = vim.split(vim.fn.getline('.'), ':')
    local filename = line_info[1]
    local line_number = tonumber(line_info[2])
    local column_number = tonumber(line_info[3])
    -- local filename, line_number, column_number = vim.fn.expand('<cfile>:p'):match('([^:]+):(%d+):(%d+)')

    vim.print(filename..line_number..column_number)
    -- Helper function to handle common logic
    local move_cursor = function()
        vim.cmd(tostring(line_number))
        vim.cmd('normal ' .. tostring(column_number) .. '|')
        vim.cmd('normal zz') -- Set the cursor to the center of the screen
    end

    -- Check if the buffer for the filename already exists
    local buffer_number = vim.fn.bufnr(filename)
    local win_id = vim.fn.win_findbuf(buffer_number)
    if ((buffer_number ~= -1) and (#win_id > 0)) then
        -- The buffer is already open, check if there's a window associated with it
        -- The window exists, switch to it and set the cursor position
        vim.api.nvim_set_current_win(win_id[1])
        move_cursor()
    else
        -- The buffer is not in a visible window, so open it in the split window
        vim.cmd('split ' .. filename)
        move_cursor()
    end

    -- Set the cursor to the center of the screen to make the line visible
    vim.cmd('normal zz')
end


-- TODO bind C-LeftMouse
map('n', '<2-LeftMouse>', GotoFileLineColumn, { desc = "Go to file at line and column based on quickfix text under cursor" })


-- vim.cmd [[
-- augroup OpenFileAtLineAndColumn
--   autocmd!
--   autocmd CursorHold * call OpenFileAtLineAndColumn()
-- augroup END
-- ]]
