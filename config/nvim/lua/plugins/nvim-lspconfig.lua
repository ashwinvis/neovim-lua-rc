-----------------------------------------------------------
-- Neovim LSP configuration file
-----------------------------------------------------------

-- Plugin: nvim-lspconfig
-- url: https://github.com/neovim/nvim-lspconfig

local lspconfig = require 'lspconfig'
local coq = require 'coq'
local lsp_flags = {
  -- This is the default in Nvim 0.7+
  debounce_text_changes = 150,
}

-- Add additional capabilities supported by coq_nvim
local capabilities = coq.lsp_ensure_capabilities(
  vim.lsp.protocol.make_client_capabilities()
)
capabilities.textDocument.completion.completionItem.documentationFormat = { 'markdown', 'plaintext' }
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.preselectSupport = true
capabilities.textDocument.completion.completionItem.insertReplaceSupport = true
capabilities.textDocument.completion.completionItem.labelDetailsSupport = true
capabilities.textDocument.completion.completionItem.deprecatedSupport = true
capabilities.textDocument.completion.completionItem.commitCharactersSupport = true
capabilities.textDocument.completion.completionItem.tagSupport = { valueSet = { 1 } }
capabilities.textDocument.completion.completionItem.resolveSupport = {
  properties = {
    'documentation', 'detail', 'additionalTextEdits', },
}
-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { silent = true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { silent = true, buffer = bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set({ 'n', 'v' }, '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', function()
    vim.lsp.buf.format({async=true})
  end, bufopts)

  -- Highlighting references
  -- https://sbulav.github.io/til/til-neovim-highlight-references/
  if client.server_capabilities.documentHighlightProvider then
    vim.api.nvim_create_augroup("lsp_document_highlight", { clear = true })
    vim.api.nvim_clear_autocmds { buffer = bufnr, group = "lsp_document_highlight" }
    vim.api.nvim_create_autocmd("CursorHold", {
      callback = vim.lsp.buf.document_highlight,
      buffer = bufnr,
      group = "lsp_document_highlight",
      desc = "Document Highlight",
    })
    vim.api.nvim_create_autocmd("CursorMoved", {
      callback = vim.lsp.buf.clear_references,
      buffer = bufnr,
      group = "lsp_document_highlight",
      desc = "Clear All the References",
    })
  end

end

local on_attach_no_diagnostics = function(client, buffer)
  on_attach(client, buffer)
  vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics, {
      -- disable virtual text
      virtual_text = false,

      -- show signs
      signs = true,

      -- delay update diagnostics
      update_in_insert = false,
    }
  )
end
--[[

Language servers setup:

For language servers list see:
https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md

--]]
-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches.
-- Add your language server below:

local servers = {
  'bashls',
  'lua_ls',
  'esbonio',
  -- 'remark_ls',
  'fortls',
  'cssls',
  'nimls',
  -- 'jedi_language_server',
  'jsonls',
  'pylsp',
  -- 'ruff_lsp'
}


-- Call setup
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
    flags = lsp_flags,
  }
end

-- NOTE: pylsp has some more plugins installable using :PylspInstall
-- https://github.com/williamboman/mason-lspconfig.nvim/blob/main/lua/mason-lspconfig/server_configurations/pylsp/README.md

-- Call setup: pylsp
local pylsp_plugins = {}
local pylsp_plugins_enabled = {}
local pylsp_plugins_disabled = {
  "pycodestyle",
  "mccabe",
  "pyflakes",
  "yapf",
}

for _, plugin in ipairs(pylsp_plugins_enabled) do
  pylsp_plugins[plugin] = { enabled = true }
end
for _, plugin in ipairs(pylsp_plugins_disabled) do
  pylsp_plugins[plugin] = { enabled = false }
end

-- NOTE: requires python-lsp-black
-- pylsp_plugins["black"] = {
--   enabled = true,
--   cache_config = true,
--   preview = true,
-- }

pylsp_plugins["ruff"] = {
  enabled = true,
  extend_select = { "I", "PLE" },
}

pylsp_plugins["mypy"] = {
  enabled = true,
  dmypy = true,
}

-- Call setup
-- NOTE: Managed by mason-lspconfig
lspconfig['pylsp'].setup {
  -- https://github.com/neovim/nvim-lspconfig/issues/96#issuecomment-748325498
  on_attach = on_attach_no_diagnostics,
  capabilities = capabilities,
  flags = lsp_flags,
  -- Server-specific settings...
  settings = {
    pylsp = {
      plugins = pylsp_plugins,
      configurationSources = { "ruff", "mypy", "flake8" },
    }
  }
}

lspconfig['lua_ls'].setup {
  on_attach = on_attach,
  capabilities = capabilities,
  flags = lsp_flags,
  -- Server-specific settings...
  settings = {
    Lua = {
      telemetry = {
        enable = false
      },
      diagnostics = {
        globals = { "vim", "use" },
      },
    }
  }
}

-- lspconfig.remark_ls.setup {
--   cmd = {os.getenv('HOME') .. ''
-- }

-- vim.lsp.set_log_level("debug")
