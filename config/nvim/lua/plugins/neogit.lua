local neogit = require("neogit")
local opts = { silent = true }

local default_ui = {kind = "vsplit"}

neogit.setup()
-- Equivalent to :Neogit
vim.keymap.set("n", "<Leader>g", function()
	neogit.open({
    kind = "tab",
    commit_popup = default_ui,
    preview_buffer = default_ui,
    popup = default_ui,
  },
  {desc="Open neogit UI"}
)
end, opts)
