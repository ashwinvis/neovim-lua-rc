M = {}
local builtin = require('telescope.builtin')

M.git_or_find_files = function(opts)
  if not pcall(builtin.git_files, opts) then
    pcall(builtin.find_files, opts)
  end
end

return M
