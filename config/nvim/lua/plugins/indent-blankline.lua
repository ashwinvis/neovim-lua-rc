-----------------------------------------------------------
-- Indent line configuration file
-----------------------------------------------------------

-- Plugin: indent-blankline
-- url: https://github.com/lukas-reineke/indent-blankline.nvim


require('ibl').setup {
  indent = {
    char = "▏",
  },
  scope = {
    char = "▏",
    show_start = true,
  },
  exclude = {
    filetypes = {
      'help',
      'git',
      'markdown',
      'text',
      'terminal',
      'lspinfo',
      'packer',
      'alpha',
      'dashboard',
      'Trouble',
      'lazy',
      'mason',
      'notify',
      'toggleterm',
    },
    buftypes = {
      'terminal',
      'nofile',
    },
  }
}
