local Terminal = require("toggleterm.terminal").Terminal
local default_shell = os.getenv("SHELL") or "bash"
-- local default_terminal = Terminal:new({ cmd = "bash", hidden = true })

-- Usage
--  <F4> opens first terminal
--  2<F4> opens second terminal and so on
function _Default_terminal_toggle()
  local default_terminal = Terminal:new(
    { cmd = default_shell, hidden = true, count = vim.v.count1 }
  )
  default_terminal:toggle()
end

function _Default_terminal_toggle_expr()
  -- https://vi.stackexchange.com/a/38374
  return "<Esc><Cmd>" .. vim.v.count .. "ToggleTerm<CR>"
end

function _IPython_toggle()
  local ipython = Terminal:new(
    { cmd = "ipython", hidden = true , count = vim.v.count1 }
  )
  ipython:toggle()
end

vim.keymap.set({"i", "n", "t"}, "<F4>", _Default_terminal_toggle)
-- vim.keymap.set({"i", "n", "t"}, "<F4>", _Default_terminal_toggle_expr, { expr = true })
vim.keymap.set({ "i", "n", "t" }, "<S-F4>", _IPython_toggle)
