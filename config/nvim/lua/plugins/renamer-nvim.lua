-- local mappings_utils = require('renamer.mappings.utils')
local renamer = require('renamer')
local keymap_opts = { silent = true }

renamer.setup()

vim.keymap.set('i', '<F2>', renamer.rename, keymap_opts)
vim.keymap.set('n', '<leader>rn', renamer.rename, keymap_opts)
vim.keymap.set('v', '<leader>rn', renamer.rename, keymap_opts)
