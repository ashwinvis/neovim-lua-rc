local map = require("core").map
local neogen = require('neogen')

neogen.setup {
  enabled = true,              --if you want to disable Neogen
  input_after_comment = true, -- (default: true) automatic jump (with insert mode) on inserted annotation
  languages = {
    python = { template = { annotation_convention = "numpydoc" } }
  }
}

-- Generate function docstrings
map("n", "<Leader>df", neogen.generate, {desc="Generate docstring for function"})
-- Generate class docstrings
map("n", "<Leader>dc", function () neogen.generate({ type = 'class' }) end, {desc="Generate docstring for class"})

-- Cycling support
map("i", "<C-l>", "<C-o>:lua require('neogen').jump_next()<CR>")
map("i", "<C-h>", "<C-o>:lua require('neogen').jump_prev()<CR>")
