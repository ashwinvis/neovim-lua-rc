-----------------------------------------------------------
-- Dashboard configuration file
-----------------------------------------------------------

-- Plugin: alpha-nvim
-- url: https://github.com/goolord/alpha-nvim

-- For configuration examples see: https://github.com/goolord/alpha-nvim/discussions/16


local alpha = require 'alpha'
local dashboard = require 'alpha.themes.dashboard'

-- Footer
local function footer()
  local version = vim.version()
  local print_version = "v" .. version.major .. '.' .. version.minor .. '.' .. version.patch
  local datetime = os.date('%Y-%m-%d %H:%M:%S')
  local hostname = os.getenv("HOST")
  if hostname == nil then
    hostname = os.getenv("USER")
  end

  return print_version .. ' ። ' .. datetime .. ' ። ' .. hostname
end

-- Banner
local banner = {
  [[                               __                ]],
  [[  ___     ___    ___   __  __ /\_\    ___ ___    ]],
  [[ / _ `\  / __`\ / __`\/\ \/\ \\/\ \  / __` __`\  ]],
  [[/\ \/\ \/\  __//\ \_\ \ \ \_/ |\ \ \/\ \/\ \/\ \ ]],
  [[\ \_\ \_\ \____\ \____/\ \___/  \ \_\ \_\ \_\ \_\]],
  [[ \/_/\/_/\/____/\/___/  \/__/    \/_/\/_/\/_/\/_/]],
}

dashboard.section.header.val = banner

-- Menu
dashboard.section.buttons.val = {
  dashboard.button('e', '  New file', ':ene <BAR> startinsert<CR>'),
  dashboard.button('r', '🕙 Recent files', ':Telescope oldfiles<CR>'),
  dashboard.button('f', '🔍 Find file', ':lua require("plugins.telescope_helpers").git_or_find_files()<CR>'),
  dashboard.button('s', '⚙  Settings', ':e $MYVIMRC | cd %:h/lua<CR>'),
  dashboard.button('p', '🔌 Open plugin manager / Lazy', ':Lazy<CR>'),
  dashboard.button('m', '⌨  Open LSP manager / Mason', ':Mason<CR>'),
  dashboard.button('u', '🔼 Update plugins', ':Lazy update<CR>'),
  dashboard.button('q', '  Quit', ':qa<CR>'),
}

dashboard.section.footer.val = footer()

alpha.setup(dashboard.config)
