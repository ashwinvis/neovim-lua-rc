require('refactoring').setup({})

-- load refactoring Telescope extension
local telescope_refactoring = require("telescope").load_extension("refactoring")

-- remap to open the Telescope refactoring menu in visual mode
vim.keymap.set("v", "<leader>rr", telescope_refactoring.refactors)
