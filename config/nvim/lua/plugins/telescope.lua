local telescope = require('telescope')
local builtin = require('telescope.builtin')
local telescope_helpers = require('plugins.telescope_helpers')

-- Required for telescope frecency extension
-- vim.g.sqlite_clib_path = "/lib64/libsqlite3.so"

-- Search for a string in your current working directory and get results live
-- as you type (respecting .gitignore)
-- See https://github.com/nvim-telescope/telescope.nvim/issues/213


vim.keymap.set('n', '<leader>f', builtin.live_grep)
vim.keymap.set('n', '<leader>p', telescope_helpers.git_or_find_files)
vim.keymap.set('n', '<leader>P', builtin.builtin)
vim.keymap.set('n', '<leader>o', builtin.treesitter)

-- Override some Vim mappings
-- remap z=
vim.keymap.set('n', 'z=', builtin.spell_suggest)
-- like cl, but a normal mode keymap
vim.keymap.set('n', '<leader>cl', builtin.quickfix)
-- like ls, but a normal mode keymap
vim.keymap.set('n', '<leader>ls', builtin.buffers)

-- Help
vim.keymap.set('n', '<leader>hm', builtin.keymaps)
vim.keymap.set('n', '<leader>hc', builtin.commands)

vim.keymap.set('n', '<leader>n', telescope.extensions.file_browser.file_browser)            -- open/close


-- Alternatively NvimTree
-- map('n', '<C-n>', ':NvimTreeToggle<CR>')            -- open/close
-- map('n', '<leader>r', ':NvimTreeRefresh<CR>')       -- refresh
-- map('n', '<leader>n', ':NvimTreeFindFile<CR>')      -- search file

-- Session / workspace
-- vim.keymap.set('n', '<leader>ws', ':SearchSession<CR>')
