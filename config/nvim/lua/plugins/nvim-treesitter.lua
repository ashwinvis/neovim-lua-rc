-----------------------------------------------------------
-- Treesitter configuration file
----------------------------------------------------------

-- Plugin: nvim-treesitter
-- url: https://github.com/nvim-treesitter/nvim-treesitter


require('nvim-treesitter.configs').setup {
  ensure_installed = { 'lua', 'python', 'javascript', 'json', 'make', 'vim', 'vimdoc', 'yaml', 'bash', 'markdown', 'markdown_inline', 'toml'},
  highlight = {
    enable = true,
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gnn",
      node_incremental = "V", --"grn",
      scope_incremental = "grc",
      node_decremental = "grm",
    },
  },
}

-- require 'nvim-treesitter.install'.compilers = {os.getenv('HOME') .. "/.nix-profile/bin/gcc"}

local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()

parser_configs.bash.filetype_to_parsername = { 'sh', 'zsh', 'bash' }

parser_configs.ecflow_def = {
  install_info = {
    url = "https://codeberg.org/ashwinvis/tree-sitter-ecflow_def.git",
    files = { "src/parser.c" },
    branch = "main",
  },
  filetype = 'def'
}

parser_configs.nim = {
  install_info = {
    url = "https://github.com/alaviss/tree-sitter-nim.git",
    files = { "src/parser.c", "src/scanner.c" },
    branch = "main",
  },
  filetype = 'nim'
}

local opt = vim.opt

opt.foldmethod = "expr"
opt.foldexpr = "nvim_treesitter#foldexpr()"
opt.foldlevel = 3
