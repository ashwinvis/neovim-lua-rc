-- Set lualine as statusline
local strings = require('plenary.strings')

local function breadcrumbs()
  local working_dir = vim.fn.getcwd()
  local dirs = vim.split(working_dir, '/', {trimempty=true, plain=true})
  local depth = vim.tbl_count(dirs)

  local dwidth = 10
  local dirs_trunc = {}

  for pos, d in ipairs(dirs) do
    local dt = strings.truncate(d, dwidth)
    table.insert(dirs_trunc, pos, dt)
  end

  local crumbs = ''
  if (depth > 3) then
    crumbs = '… ❯ '..table.concat(dirs_trunc, ' ❯ ', depth - 2)
  else
    crumbs = table.concat(dirs_trunc, ' ❯ ')
  end

  local width = 10*3+2
  return strings.align_str(crumbs, width, true)
end

require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'auto',
    component_separators = '|',
    section_separators = '',
  },
  sections = {
    -- lualine_a = {'mode'},
    -- lualine_b = {'branch', 'diff', 'diagnostics'},
    lualine_c = {'nvim_treesitter#statusline'},
    -- lualine_x = {'encoding', 'fileformat', 'filetype'},
    -- lualine_y = {'progress'},
    -- lualine_z = {'location'}
  },
  tabline = {
    lualine_a = {breadcrumbs},
    lualine_b = {'buffers'},
    lualine_c = {},
    lualine_x = {},
    lualine_y = {},
    lualine_z = {'tabs'},
  },
}
