-----------------------------------------------------------
-- Plugin manager configuration file
-----------------------------------------------------------

-- Plugin manager: packer.nvim
-- url: https://github.com/wbthomason/packer.nvim

-- For information about installed plugins see the README
--- neovim-lua/README.md
--- https://github.com/brainfucksec/neovim-lua#readme


local cmd = vim.cmd
cmd [[packadd packer.nvim]]

local packer = require 'packer'

-- Add packages
packer.startup(function(use)
  use 'wbthomason/packer.nvim' -- packer can manage itself

  -- Indent line
  use 'lukas-reineke/indent-blankline.nvim'

  -- Autopair
  use {
    'windwp/nvim-autopairs',
    config = function()
      require('nvim-autopairs').setup()
    end
  }

  -- Surround
  use {
    "kylechui/nvim-surround",
    tag = "*", -- Use for stability; omit to use `main` branch for the latest features
    config = function()
      require("nvim-surround").setup({
        -- Configuration here, or leave empty to use defaults
      })
    end
  }
  -- Tag viewer
  -- use 'liuchengxu/vista.vim'

  -- Treesitter interface
  use 'nvim-treesitter/nvim-treesitter'

  -- Color schemes
  use 'navarasu/onedark.nvim'

  -- use 'tanvirtin/monokai.nvim'

  use 'ellisonleao/gruvbox.nvim'

  -- LSP
  use {
    'neovim/nvim-lspconfig',
    requires = {
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      "jose-elias-alvarez/null-ls.nvim",
    },
    config = function()
      require("mason").setup()
      require("mason-lspconfig").setup()
    end
  }

  -- Rust
  -- use {
  --   'simrat39/rust-tools.nvim',
  --   requires = {
  --     -- Debugging
  --     'nvim-lua/plenary.nvim',
  --     'mfussenegger/nvim-dap'
  --   },
  -- }

  -- Autocomplete
  --[[
  use {
    'hrsh7th/nvim-cmp',
    requires = {
      'L3MON4D3/LuaSnip',
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-buffer',
      'saadparwaiz1/cmp_luasnip',
    },
  }
  ]]
  vim.g.coq_settings = { auto_start = 'shut-up' }

  use {
    'ms-jpq/coq_nvim',
    branch = 'coq',
    requires = {
      {
        'ms-jpq/coq.artifacts',
        branch = 'artifacts',
      },
      {
        'ms-jpq/coq.thirdparty',
        branch = '3p',
      },
    },
    run = ':COQdeps'
  }


  -- Renamer

  -- use {
  --   'filipdutescu/renamer.nvim',
  --   branch = 'master',
  --   requires = { { 'nvim-lua/plenary.nvim' } }
  -- }

  -- Statusline
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'nvim-tree/nvim-web-devicons', opt = true }
  }


  -- git labels
  use {
    'lewis6991/gitsigns.nvim',
    requires = { 'nvim-lua/plenary.nvim' },
    config = function()
      require('gitsigns').setup()
    end
  }

  -- Dashboard (start screen)
  use 'goolord/alpha-nvim'

  -- Docstrings
  use {
    "danymat/neogen",
    requires = "nvim-treesitter/nvim-treesitter",
    config = function()
      require('neogen').setup {}
    end,
    -- Uncomment next line if you want to follow only stable versions
    -- tag = "*"
  }

  -- Comment
  use {
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end
  }

  -- Git
  use {
    'TimUntersberger/neogit',
    config = function()
      require('neogit').setup()
    end
  }

  -- VSCode like experience, but with <leader>key
  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      { 'nvim-lua/plenary.nvim' },
    }
  }

  -- VSCode like refactoring
  use {
    "ThePrimeagen/refactoring.nvim",
    requires = {
      { "nvim-lua/plenary.nvim" },
      { "nvim-treesitter/nvim-treesitter" }
    }
  }

  use {
    "nvim-telescope/telescope-frecency.nvim",
    config = function()
      require("telescope").load_extension("frecency")
    end,
    requires = {"tami5/sqlite.lua"}
  }

  -- File explorer
  use {
    "nvim-telescope/telescope-file-browser.nvim",
    config = function()
      require("telescope").load_extension("file_browser")
    end
  }
  -- use 'nvim-tree/nvim-tree.lua'

  -- Session and workspaces
  -- use {
  --   'rmagatti/session-lens',
  --   requires = {'rmagatti/auto-session', 'nvim-telescope/telescope.nvim'},
  --   config = function()
  --     require('session-lens').setup({--[[your custom config--]]})
  --     require("telescope").load_extension("session-lens")
  --   end
  -- }



  -- Terminal
  use { "akinsho/toggleterm.nvim", tag = '*', config = function()
    require("toggleterm").setup()
  end }

end)

-- Automatically source and re-compile packer whenever you save this init.lua
local packer_group = vim.api.nvim_create_augroup('Packer', { clear = true })
vim.api.nvim_create_autocmd('BufWritePost', {
  command = 'source <afile> | silent! LspStop | silent! LspStart | PackerCompile',
  group = packer_group,
  pattern = vim.fs.dirname(vim.fn.expand '$MYVIMRC').."/**",
})
