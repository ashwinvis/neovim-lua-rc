--[[

Neovim init file
Version: 2023.10
Maintainer: ashwinvis
Website: https://codeberg.org/ashwinvis/neovim-lua-rc

--]]
local g = vim.g -- Global variables

-- Import Lua modules

-- Execute `lcd %:h/lua` to easily switch using `gf`
if g.vscode then
  require("vscode_init")
  require("core/keymaps")
elseif g.neovide then
  require("core/settings")
  require("core/keymaps")
  require("neovide_init")
else
  require("core/settings")
  require("core/keymaps")
  -- require("packer_init")
  require("lazy_init")

  -------------------------------------------------------------
  -- Colorscheme
  -------------------------------------------------------------
  local opt = vim.opt
  local hour = tonumber(os.date("%H"))
  local light_theme = (hour > 6 and hour < 18)
  local color_plugin = "nightfox" -- onedark
  local color_variant = color_plugin

  if light_theme then
    opt.background = "light"
    color_variant = "dayfox"
  else
    opt.background = "dark"
    color_variant = "carbonfox"
  end

  local ok, colorscheme = pcall(require, color_plugin)
  if ok then
    vim.cmd("colorscheme "..color_variant)
    -- colorscheme.load()  -- for onedark
    colorscheme.setup {}
  end
end
