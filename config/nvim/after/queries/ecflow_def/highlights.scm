[
    "extern"
    "suite"
    "endsuite"
    "family"
    "endfamily"
    "task"
    "event"
    "defstatus"
    "edit"
    "repeat"
    "trigger"
    "date"
    "cron"
] @keyword

(identifier) @variable

(edit_type) @builtin

(comment) @comment

[
    (time_type)
    (number)
] @number

[
    (boolean_operator)
    (comparison_operator)
] @operator

(string_type) @string
